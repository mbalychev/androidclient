package com.lmp.ParentMessenger.Background;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

class PMService extends Service implements IAppManager {

    private String parentUniqueId;
    private String password;
    private boolean authenticatedUser = false;
    private final int UPDATE_TIME_PERIOD = 120000;
    ScheduledExecutorService mScheduler;


    public static final String NOTIFICATIONS_LIST_UPDATED = "Notifications list updated";


    private NotificationManager mNM;
    private final IBinder mBinder = new PMBinder();

    public class PMBinder extends Binder {
    public IAppManager getService() {
        return PMService.this;
    }

}   @Override
    public void onCreate(){
        mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        mScheduler = Executors.newSingleThreadScheduledExecutor();
    }

    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void exit() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String GetNotifications(){
        // Main logic for getting new notifications here
        return null;
    }

    public String authenticateUser(String parentUniqueId, String passwordText)
    {
        this.parentUniqueId = parentUniqueId;
        this.password = passwordText;

        this.authenticatedUser = false;

        String result = null;// authenticateUser(parentUniqueId, passwordTExt)
        if (result != null /*&& !result.equals(Login.AUTHENTICATION_FAILED)*/)
        {
            this.authenticatedUser = true;

            mScheduler.scheduleAtFixedRate
                    (new Runnable() {
                        public void run() {
                            try {
                                Intent i = new Intent(NOTIFICATIONS_LIST_UPDATED);
                                String tmp = PMService.this.GetNotifications();
                                if (tmp != null) {
                                    sendBroadcast(i);
                                    Log.i("notification list broadcast sent ", "");
                                }
                                else {
                                    Log.i("notification list returned null", "");
                                }
                            }
                            catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 0, 2, TimeUnit.MINUTES);

        }

        return result;
    }
}

